/**
 *
 */

var body = document.querySelector('body')
  , socket;

var app = {
    
    // Application Constructor
    initialize: function() {
        app.bindEvents();
        
    },

    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
        document.addEventListener('pause', this.onPause, false);
        document.addEventListener('resume', this.onResume, false);
    },

    // Connect with socket
    reconnectSocket: function () {
        var el = document.createElement('div')
          , s = socket.socket;

        el.innerHTML = "<b>reconnecting ...</b>";
        
        body.appendChild(el);

        s.reconnect();

        //app.socketBindEvents();
    },

    // Socket binds.
    socketBindEvents: function () {
        socket.on('connected', app.onConnected.bind(app));
    },
    
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicity call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        var div = document.createElement('div');

        socket = io.connect('http://192.168.0.100:3000');

        app.socketBindEvents();

        div.innerHTML = "Device Ready !";
        body.appendChild(div);
    },

    // onConnected Socket Event Handler
    onConnected: function (data) {
        console.log('on socket connected: ', data);

        var el = document.createElement('div');

        el.innerHTML = data.message;
        
        body.appendChild(el);

        socket.emit('my other event', {
            my: 'data'
        });
    },

    // resume Event Handler
    onResume: function () {
        console.log('on resume');

        setTimeout(function() {
            app.reconnectSocket();    
        }, 0);

        var div = document.createElement('div');
        div.innerHTML = "On resume";

        body.appendChild(div);
    },

    // on pause
    onPause: function () {
        console.log('on pause');

        var div;

        // remove all listeners
        socket.disconnect();
        //socket.removeAllListeners();

        div = document.createElement('div');
        div.innerHTML = "On pause";

        body.appendChild(div);
    }
};
